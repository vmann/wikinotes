from jinja2 import PackageLoader
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import Response
from starlette.routing import Route, Mount
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
from tortoise import fields, Tortoise
from tortoise.models import Model
from uvicorn import Config, Server
from uvloop import run


class Note(Model):
    id = fields.UUIDField(pk=True)
    paragraphs: fields.ReverseRelation["Paragraph"]


class Paragraph(Model):
    id = fields.UUIDField(pk=True)
    note: fields.ForeignKeyRelation[Note] = fields.ForeignKeyField(
        "model.Note", related_name="paragraphs"
    )
    content = fields.TextField(default="")


templates = Jinja2Templates(
    directory="/nonexistent",  # just a dummy, FileSystemLoader is not used
    loader=PackageLoader("wikinotes"),
)


async def home(request: Request) -> Response:
    return templates.TemplateResponse(
        "home.html",
        headers={
            "HX-Push-Url": str(request.url_for("home")),
        },
        context={
            "request": request,
        },
    )


async def create_note(request: Request) -> Response:
    note = await Note.create()
    await note.fetch_related("paragraphs")
    return templates.TemplateResponse(
        "note.html",
        headers={
            "HX-Push-Url": str(request.url_for("note", note_id=note.id)),
        },
        context={
            "request": request,
            "note": note,
        },
    )


async def note(request: Request) -> Response:
    note = await Note.get(id=request.path_params["note_id"])
    await note.fetch_related("paragraphs")
    return templates.TemplateResponse(
        "note.html",
        headers={
            "HX-Push-Url": str(request.url_for("note", note_id=note.id)),
        },
        context={
            "request": request,
            "note": note,
        },
    )


async def create_paragraph(request: Request) -> Response:
    note = await Note.get(id=request.path_params["note_id"])
    async with request.form() as form:
        paragraph = await Paragraph.create(note=note, content=str(form["content"]))
    return templates.TemplateResponse(
        "paragraph.html",
        context={
            "request": request,
            "note": note,
            "paragraph": paragraph,
        },
    )


async def paragraph(request: Request) -> Response:
    note = await Note.get(id=request.path_params["note_id"])
    paragraph = await Paragraph.get(id=request.path_params["paragraph_id"])
    if request.method == "PUT":
        async with request.form() as form:
            await paragraph.update_from_dict({"content": str(form["content"])}).save()
    return templates.TemplateResponse(
        "paragraph.html",
        context={
            "request": request,
            "note": note,
            "paragraph": paragraph,
        },
    )


async def edit_paragraph(request: Request) -> Response:
    note = await Note.get(id=request.path_params["note_id"])
    paragraph = await Paragraph.get(id=request.path_params["paragraph_id"])
    return templates.TemplateResponse(
        "edit.html",
        context={
            "request": request,
            "note": note,
            "paragraph": paragraph,
        },
    )


app = Starlette(
    debug=True,
    routes=[
        Route("/", home, name="home"),
        Route(
            "/note",
            create_note,
            name="create_note",
            methods=["POST"],
        ),
        Route("/note/{note_id:uuid}", note),
        Route(
            "/note/{note_id:uuid}/paragraph",
            create_paragraph,
            name="create_paragraph",
            methods=["POST"],
        ),
        Route(
            "/note/{note_id:uuid}/paragraph/{paragraph_id:uuid}",
            paragraph,
            name="paragraph",
            methods=["GET", "PUT"],
        ),
        Route(
            "/note/{note_id:uuid}/paragraph/{paragraph_id:uuid}/edit",
            edit_paragraph,
            name="edit_paragraph",
        ),
        Mount("/static", app=StaticFiles(packages=["wikinotes"]), name="static"),
    ],
)


async def async_main() -> None:
    await Tortoise.init(db_url="sqlite://:memory:", modules={"model": ["wikinotes"]})
    await Tortoise.generate_schemas()

    config = Config("wikinotes:app", port=8080, log_level="info")
    server = Server(config)
    await server.serve()

    await Tortoise.close_connections()


def main() -> None:
    run(async_main())
